use crate::config::Config;
use crate::request_handler::handle_request;
use crate::response_handler::handle_response;
use crate::ssl_manager::{get_host_pair, is_ssl_tls_handshake};
use crate::{is_auth, wrap};
use crate::sqlite_manager::{Speaker, Msg};

use nomhttp::http_header::HttpHeader;
use nomhttp::http_method::HttpMethod;
use nomhttp::http_request::HttpRequest;
use nomhttp::http_response::HttpResponse;

use core::pin::Pin;
use log::{debug, error, info};
use nom_bufreader::bufreader::BufReader;
use openssl::{
    pkey::{PKey, Private},
    ssl::{HandshakeError, Ssl, SslAcceptor, SslConnector, SslMethod, SslStream, SslVerifyMode},
    x509::X509,
};
use rusqlite::Connection;
use std::{
    error::Error,
    io::{Read, Write},
    net::{IpAddr, Ipv4Addr, TcpStream, ToSocketAddrs},
    time::Instant,
    sync::mpsc,
};

pub fn client_manager(
    s: TcpStream,
    ca_cert: X509,
    ca_key_pair: PKey<Private>,
    config: Config,
    sender: mpsc::Sender<Msg>
) -> Result<(), Box<dyn Error>> {
    if config.force_target().is_empty() {
        let mut reader = BufReader::with_capacity(1024 * 1024, s);
        let Some(req) = handle_request(&mut reader) else {
            return Err("Handle request returned None".into());
        };
        let (method, path, version, headers, body, raw) = req;
        let mut req = HttpRequest::new(method, path, version, headers, body, raw, false);
        debug!("{:?}", req);
        if config.enable_auth() {
            if !is_auth!(req, config.shared_secret()) {
                debug!("Client not authentified or authentication is invalid");
                return Err("Client not authentified or authentication is invalid ".into());
            }
            let headers = req.headers_mut();
            headers.retain(|h| h.name() != "rp_auth");
        }
        debug!("{:?}", req);
        /* prevent attacking 127.0.0.1 */
        if !config.allow_localhost() {
            let mut mask = req
                .destination()
                .ok_or(format!(
                    "Request's destination wasn't valid: {:?}",
                    req.destination()
                ))?
                .to_socket_addrs()?;

            if req.path().contains("127.0.0.1")
                || mask.any(|x| x.ip() == IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)))
            {
                return Err("Incoming request ".into());
            }
        }

        if let HttpMethod::Connect = req.method() {
            /* https */
            debug!("New HTTPS request !");
            handle_https_request(reader, req, ca_cert, ca_key_pair, config, sender)
        } else {
            /* http */
            debug!("New HTTP request !");
            debug!("{:?}", req.headers());
            handle_cleartext_request(reader, req, config, sender)
        }
    } else {
        let now = Instant::now();
        let remote_addr = format!("{}", s.peer_addr()?);

        let host_no_port = if config.force_target().contains(':') {
            config.force_target().split(':').take(1).collect()
        } else {
            config.force_target().to_string()
        };

        let mut buff: [u8; 512] = [0; 512];

        s.peek(&mut buff)?;

        debug!("buff: {:?}", &buff);
        if is_ssl_tls_handshake(&buff) {
            /* SSL handshake */
            debug!("This is HTTPs request");
            let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
            let (cert, pair) = get_host_pair(&conn, &host_no_port, &ca_cert, &ca_key_pair)?;
            let mut sslstream = wrap!(s, &cert, &pair, ca_cert);
            debug!("Accepting ssl stream");
            Pin::new(&mut sslstream).accept()?;
            debug!("SSL Handshake done");
            let mut reader = BufReader::with_capacity(1024 * 1024, sslstream);
            let Some(req) = handle_request(&mut reader) else {
                return Err("Handle request returned None".into());
            };
            let (method, path, version, headers, body, raw) = req;
            let mut req = HttpRequest::new(method, path, version, headers, body, raw, true);
            debug!("{:?}", req);
            if config.enable_auth() {
                if !is_auth!(req, config.shared_secret()) {
                    debug!("Client not authentified or authentication is invalid");
                    return Err("Client not authentified or authentication is invalid ".into());
                }
                let headers = req.headers_mut();
                headers.retain(|h| h.name() != "rp_auth");
            }
            debug!("Received request from client via SSLStream");
            let mut rstream_std = TcpStream::connect(config.force_target())?;
            let resp = match config.force_https() {
                true => {
                    let mut rstream = ssl_connect(&rstream_std, &host_no_port)?;
                    rstream.write_all(&req.as_bytes())?;
                    debug!("SSL handshake to destination is done");
                    let mut rstreamreader= BufReader::with_capacity(1024 * 1024, rstream);
                    let resp = match handle_response(&mut rstreamreader, reader.into_inner()) {
                        Ok(r) => r,
                        Err(e) => {
                            error!("handle_response failed for request: {}", &req);
                            return Err(e);
                        }
                    };
                    resp
                }
                false => {
                    rstream_std.write_all(&req.as_bytes())?;
                    let mut rstreamreader= BufReader::with_capacity(1024 * 1024, rstream_std);
                    let resp = match handle_response(&mut rstreamreader, reader.into_inner()) {
                        Ok(r) => r,
                        Err(e) => {
                            error!("handle_response failed for request: {}", &req);
                            return Err(e);
                        }
                    };
                    resp
                }
            };

            info!(
                "{} {} [{}] - {:?}",
                req.method(),
                req.path(),
                resp.status(),
                now.elapsed()
            );
            if !config.is_scope_enabled() || (config.is_scope_enabled() && req.destination().is_some() && req.destination().unwrap().contains(config.scope())) {
                debug!("IB: {:?}", req.destination());
                insert_complete_request(
                    &req,
                    &resp,
                    &remote_addr,
                    true,
                    &sender,
                    &now.elapsed(),
                )?;
            } else {
                debug!("OOB: {:?}", req.destination());
            }

            /* websockets maybe ? */
            Ok(())
        } else {
            debug!("This is standard HTTP request");
            let mut reader = BufReader::with_capacity(1024 * 1024, s);
            let Some(req) = handle_request(&mut reader) else {
                return Err("Handle request returned None".into());
            };
            let (method, path, version, headers, body, raw) = req;
            let req = HttpRequest::new(method, path, version, headers, body, raw, false);
            let s = reader.into_inner();
            debug!("{}", req);
            let mut rstream_std = TcpStream::connect(config.force_target())?;
            debug!("Connected to destination");
            let resp = match config.force_https() {
                true => {
                    let mut rstream = ssl_connect(&rstream_std, &host_no_port)?;
                    rstream.write_all(&req.as_bytes())?;
                    debug!("SSL handshake to destination is done");
                    let mut rstreamreader= BufReader::with_capacity(1024 * 1024, rstream);
                    let resp = match handle_response(&mut rstreamreader, s) {
                        Ok(r) => r,
                        Err(e) => {
                            error!("handle_response failed for request: {}", &req);
                            return Err(e);
                        }
                    };
                    resp
                }
                false => {
                    rstream_std.write_all(&req.as_bytes())?;
                    let mut rstreamreader= BufReader::with_capacity(1024 * 1024, rstream_std);
                    let resp = match handle_response(&mut rstreamreader, s) {
                        Ok(r) => r,
                        Err(e) => {
                            error!("handle_response failed for request: {}", &req);
                            return Err(e);
                        }
                    };
                    resp
                }
            };
            info!(
                "{} {} [{}] - {:?}",
                req.method(),
                req.path(),
                resp.status(),
                now.elapsed()
            );
            if !config.is_scope_enabled() || (config.is_scope_enabled() && req.destination().is_some() && req.destination().unwrap().contains(config.scope())) {
                debug!("IB: {:?}", req.destination());
                insert_complete_request(
                    &req,
                    &resp,
                    &remote_addr,
                    false,
                    &sender,
                    &now.elapsed(),
                )?;
            } else {
                debug!("OOB: {:?}", req.destination());
            }
            // websockets maybe ?
            Ok(())
        }
    }
}

fn handle_https_request(
    reader: BufReader<TcpStream>,
    req: HttpRequest,
    ca_cert: X509,
    ca_key_pair: PKey<Private>,
    config: Config,
    sender: mpsc::Sender<Msg>
) -> Result<(), Box<dyn Error>> {
    debug!("Entering handle_https_request");
    let now = Instant::now();
    let destination = req
        .destination()
        .ok_or("Could not get destination from request")?;
    debug!("Client wants to join {}", destination);
    let mut s = reader.into_inner();
    let remote_addr = format!("{}", s.peer_addr()?);
    // accept tunnel
    s.write_all(
        "HTTP/1.1 200 OK\r\nProxy-Connection: Close\r\nConnection: Close\r\n\r\n".as_bytes(),
    )?;
    debug!("Accepted HTTPS tunneling");
    let mut host_no_port = req
        .destination_base()
        .ok_or("Error parsing host")?
        .to_owned();

    if host_no_port.contains(':') {
        host_no_port = host_no_port.split(':').take(1).collect();
    }

    let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
    let (cert, pair) = get_host_pair(&conn, &host_no_port, &ca_cert, &ca_key_pair)?;
    let mut sslstream = wrap!(s, &cert, &pair, ca_cert);
    Pin::new(&mut sslstream).accept()?;
    debug!("Standard TCPStream is now wrapped with SslStream and handshake is over");
    let mut reader = BufReader::with_capacity(1024 * 1024, sslstream);
    let Some(req) = handle_request(&mut reader) else {
        return Err("Handle request returned None".into());
    };
    let (method, path, version, headers, body, raw) = req;
    let mut req = HttpRequest::new(method, path, version, headers, body, raw, false);

    if let Some(h) = req.headers_mut().iter_mut().find(|x| x.name().to_lowercase() == "connection") {
        *h = HttpHeader::new("Connection".into(), "close".into());
        debug!("Changed connection header to: {}", h.value());
    }

    if let Some(h) = req.headers_mut().iter_mut().find(|x| x.name().to_lowercase() == "accept-encoding" && x.value().to_lowercase().contains("gzip")) {
        *h = HttpHeader::new("Accept-Encoding".into(), "deflate".into());
        debug!("Changed encoding header to: {}", h.value());
    }

    if let HttpMethod::Other(_) = req.method() {
        return Err("Failed to parse HTTP Method or the result was invalid".into());
    }
    debug!("Received request from client via SSLStream");
    let rstream_std = if config.force_target().is_empty() {
        TcpStream::connect(destination)?
    } else {
        TcpStream::connect(config.force_target())?
    };
    let mut rstream = ssl_connect(&rstream_std, &host_no_port)?;
    rstream.write_all(&req.as_bytes())?;
    debug!("SSL handshake to destination is done");
    let mut rstreamreader = BufReader::with_capacity(1024 * 1024, rstream);
    let mut csws = reader.get_mut();
    let resp = match handle_response(&mut rstreamreader, &mut csws) {
        Ok(r) => r,
        Err(e) => {
            error!("handle_response failed for request: {}", &req);
            return Err(e);
        }
    };
    info!(
        "{} {} [{}] - {:?}",
        req.method(),
        req.path(),
        resp.status(),
        now.elapsed()
    );
    let rsws = rstreamreader.get_mut();
    if !config.is_scope_enabled() || (config.is_scope_enabled() && req.destination().is_some() && req.destination().unwrap().contains(config.scope())) {
        debug!("IB: {:?}", req.destination());
        insert_complete_request(
            &req,
            &resp,
            &remote_addr,
            true,
            &sender,
            &now.elapsed(),
        )?;
    } else {
        debug!("OOB: {:?}", req.destination());
    }
    /* websockets maybe ? */
    if req.headers().iter().any(|e| e.name() == "Upgrade" && e.value() == "websocket") && resp.status().code() == 101 {
        debug!("Client and Server handshaked, it's now websocket");
        handle_wss(csws, rsws, sender)?;
    }
    if let Err(e) = reader.get_mut().shutdown() {
        return Err(format!("Could not close socket: {}", e).into());
    }

    if let Err(e) = rstreamreader.get_mut().shutdown() {
        return Err(format!("Could not close socket: {}", e).into());
    }

    Ok(())
}

fn handle_cleartext_request(
    reader: BufReader<TcpStream>,
    mut req: HttpRequest,
    config: Config,
    sender: mpsc::Sender<Msg>
) -> Result<(), Box<dyn Error>> {
    let now = Instant::now();
    let destination = req
        .destination()
        .ok_or("Couldn't get destination from request")?;
    let s = reader.get_ref();
    let remote_addr = format!("{}", s.peer_addr()?);
    let mut rstream = if config.force_target().is_empty() {
        TcpStream::connect(destination)?
    } else {
        TcpStream::connect(config.force_target())?
    };
    let mut csws = s.try_clone()?;
    let mut rsws = rstream.try_clone()?;
    let pattern = format!("http://{}/", req.destination_base().ok_or("")?);
    debug!("req.destination_base(): {:?}, pattern: {}, req.path(): {}, req.path().starts_with(&pattern): {}", req.destination_base(), pattern, req.path(), req.path().starts_with(&pattern));
    let mut bytes: Vec<u8>;
    if let HttpMethod::Other(_) = req.method() {
        return Err("Failed to parse HTTP Method from request or the result was invalid".into());
    }
    if let Some(h) = req.headers_mut().iter_mut().find(|x| x.name().to_lowercase() == "connection") {
        *h = HttpHeader::new("Connection".into(), "close".into());
        debug!("Changed connection header to: {}", h.value());
    }

    if let Some(h) = req.headers_mut().iter_mut().find(|x| x.name().to_lowercase() == "accept-encoding" && x.value().to_lowercase().contains("gzip")) {
        *h = HttpHeader::new("Accept-Encoding".into(), "deflate".into());
        debug!("Changed encoding header to: {}", h.value());
    }

    if req.path().starts_with(&pattern) {
        /* need to patch request */
        let status_line = format!(
            "{} /{} {}\r\n",
            req.method(),
            &req.path()[pattern.len()..],
            req.version(),
        );
        req.set_path(&format!("/{}", &req.path()[pattern.len()..]));
        debug!("Patched status_line: {}", status_line);
        let default_status_line =
            format!("{} {} {}\r\n", req.method(), &req.path(), req.version(),);

        bytes = status_line.as_bytes().to_vec();
        bytes.extend_from_slice(&req.as_bytes()[default_status_line.len()..]);
    } else {
        bytes = req.as_bytes().to_vec();
    }
    debug!("REQ: {:?}", &bytes);

    rstream.write_all(&bytes)?;
    let mut rstreamreader= BufReader::with_capacity(1024 * 1024, rstream);

    let resp = match handle_response(&mut rstreamreader, s) {
        Ok(r) => r,
        Err(e) => {
            error!("handle_response failed for request: {}", &req);
            return Err(e);
        }
    };
    info!(
        "{} {} [{}] - {:?}",
        req.method(),
        req.path(),
        resp.status(),
        now.elapsed()
    );
    if !config.is_scope_enabled() || (config.is_scope_enabled() && req.destination().is_some() && req.destination().unwrap().contains(config.scope())) {
        debug!("IB: {:?}", req.destination());
        insert_complete_request(
            &req,
            &resp,
            &remote_addr,
            false,
            &sender,
            &now.elapsed(),
        )?;
    } else {
        debug!("OOB: {:?}", req.destination());
    }
    /* websockets maybe ? */
    if req.headers().iter().any(|e| e.name() == "Upgrade" && e.value() == "websocket") && resp.status().code() == 101 {
        debug!("Client and Server handshaked, it's now websocket");
        handle_ws(&mut csws, &mut rsws, sender)?;
    }

    if let Err(e) = reader.get_ref().shutdown(std::net::Shutdown::Both) {
        return Err(format!("Could not close socket: {}", e).into());
    }

    if let Err(e) = rstreamreader.get_mut().shutdown(std::net::Shutdown::Both) {
        return Err(format!("Could not close socket: {}", e).into());
    }

    Ok(())
}

fn ssl_connect<'a>(
    rstream_std: &'a TcpStream,
    host_no_port: &str,
) -> Result<openssl::ssl::SslStream<&'a TcpStream>, Box<dyn Error>> {
    let mut err_count = 0;
    let mut connectorb = SslConnector::builder(SslMethod::tls())?;
    connectorb.set_verify(SslVerifyMode::NONE);
    let connector = connectorb.build();

    let rstream = loop {
        if err_count > 3 {
            return Err(format!(
                "Abort: Too many failures during SSL handshake with: {}",
                &host_no_port
            )
            .into());
        }
        match connector.connect(host_no_port, rstream_std) {
            Ok(n) => break n,
            Err(HandshakeError::WouldBlock(eb)) => {
                err_count += 1;
                error!("Another WouldBlock: {:?}", eb);
                continue;
            }
            Err(HandshakeError::SetupFailure(err)) => {
                return Err(format!(
                    "Abort: SetupFailure mid SSL handshake with: {} [{:?}]",
                    &host_no_port, err
                )
                .into());
            }
            Err(_) => {
                err_count += 1;
                continue;
            }
        }
    };

    Ok(rstream)
}

fn insert_complete_request(
    req: &HttpRequest,
    resp: &HttpResponse,
    remote_addr: &String,
    ssl: bool,
    sender: &mpsc::Sender<Msg>,
    response_time: &std::time::Duration,
) -> Result<(), Box<dyn Error>> {

    let msg = Msg::WebData(req.clone(), resp.clone(), remote_addr.to_string(), ssl, *response_time);
    sender.send(msg)?;
    Ok(())
}

fn insert_websocket_message(
    direction: &String,
    data: &[u8],
    sender: &mpsc::Sender<Msg>,
    speaker: Speaker,
    ssl: bool
) -> Result<(), Box<dyn Error>> {
    let msg = Msg::WebSocketData(direction.to_string(), speaker, ssl, data.to_vec());
    sender.send(msg)?;
    Ok(())
}

fn handle_ws(csws: &mut TcpStream, rsws: &mut TcpStream, sender: mpsc::Sender<Msg>) -> Result<(), Box<dyn Error>>
{

    csws.set_nonblocking(true)?;
    rsws.set_nonblocking(true)?;

    let direction = format!("{} <-> {}", csws.peer_addr()?, rsws.peer_addr()?);
    loop {
        let mut buff: [u8; 2048] = [0; 2048];

        match csws.read(&mut buff) {
            Ok(n) => {
                if n >= 1 {
                    /* we got data */
                    rsws.write_all(&buff[..n])?;
                    let s = String::from_utf8_lossy(&buff[..n]).to_string();
                    let timestamp = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH)?;
                    debug!("[WS] {:?} - CSWS Says: {:?}", timestamp, s);
                    insert_websocket_message(&direction, &buff[..n], &sender, Speaker::Client, false)?;
                }

            },
            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                /* socket not ready, we wait */
            },
            Err(e) => {
                debug!("[WS] Got error {} while reading csws", e);
                return Err(e.into());
            },
        }

        match rsws.read(&mut buff) {
            Ok(n) => {
                if n >= 1 {
                    /* we got data */
                    csws.write_all(&buff[..n])?;
                    let s = String::from_utf8_lossy(&buff[..n]).to_string();
                    let timestamp = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH)?;
                    debug!("[WS] {:?} - RSWS Says: {:?}", timestamp, s);
                    insert_websocket_message(&direction, &buff[..n], &sender, Speaker::Server, false)?;
                }
            },
            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                /* socket not ready, we wait */
            },
            Err(e) => {
                debug!("[WS] Got error {} while reading rsws", e);
                return Err(e.into());
            },
        }
        std::thread::sleep(std::time::Duration::from_millis(30));
    }
}


fn handle_wss(csws: &mut SslStream<&TcpStream>, rsws: &mut SslStream<&TcpStream>, sender: mpsc::Sender<Msg>) -> Result<(), Box<dyn Error>>
{
    csws.get_mut().set_nonblocking(true)?;
    rsws.get_mut().set_nonblocking(true)?;

    let direction = format!("{} <-> {}", csws.get_ref().peer_addr()?, rsws.get_ref().peer_addr()?);

    loop {
        let mut buff: [u8; 2048] = [0; 2048];

        match csws.read(&mut buff) {
            Ok(n) => {
                if n >= 1 {
                    /* we got data */
                    rsws.write_all(&buff[..n])?;
                    let s = String::from_utf8_lossy(&buff[..n]).to_string();
                    let timestamp = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH)?;
                    debug!("[WSS] {:?} - CSWS Says: {:?}", timestamp, s);
                    insert_websocket_message(&direction, &buff[..n], &sender, Speaker::Client, true)?;
                }
            },
            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                /* socket not ready, we wait */
            },
            Err(e) => {
                debug!("[WSS] Got error {} while reading csws", e);
                return Err(e.into());
            },
        }

        match rsws.read(&mut buff) {
            Ok(n) => {
                if n >= 1 {
                    /* we got data */
                    csws.write_all(&buff[..n])?;
                    let s = String::from_utf8_lossy(&buff[..n]).to_string();
                    let timestamp = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH)?;
                    debug!("[WSS] {:?} - RSWS Says: {:?}", timestamp, s);
                    insert_websocket_message(&direction, &buff[..n], &sender, Speaker::Server, true)?;
                }
            },
            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                /* socket not ready, we wait */
            },
            Err(e) => {
                debug!("[WSS] Got error {} while reading rsws", e);
                return Err(e.into());
            },
        }

        std::thread::sleep(std::time::Duration::from_millis(30));

    }
}