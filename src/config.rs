use clap::Parser;

/// MITM Proxy server that stores requests in sqlite for later usage
#[derive(Parser, Debug, Clone, Default)]
pub struct Config {
    /// Path for the project to be stored
    #[clap(short, long)]
    directory: String,

    /// addr to bind to
    #[clap(short, long, default_value = "127.0.0.1")]
    addr: String,

    /// port to bind to
    #[clap(short, long, default_value = "8000")]
    port: usize,

    /// allow debug logs
    #[clap(short, long)]
    verbose: bool,

    /// shared secret for the API
    #[clap(short, long)]
    secret: String,

    /// disable localhost protection
    #[clap(long)]
    allow_localhost: bool,

    /// force target for any incoming request
    #[clap(long, default_value = "")]
    force_target: String,

    /// forced target uses https ?
    #[clap(long = "--force-https")]
    force_https: bool,

    /// addr for the api
    #[clap(short = 'A', long, default_value = "127.0.0.1")]
    api_addr: String,

    /// port for the api
    #[clap(short = 'P', long, default_value = "8443")]
    api_port: usize,

    ///
    #[clap(long)]
    enable_paging: bool,

    /// add auth for everything
    #[clap(long)]
    enable_auth: bool,

    /// add a scope to prevent registering out of scope stuff
    #[clap(short = 'S', long, default_value = "")]
    scope: String,
}

impl Config {
    pub fn directory(&self) -> &String {
        &self.directory
    }

    pub fn full_addr(&self) -> String {
        format!("{}:{}", self.addr, self.port)
    }

    pub fn debug(&self) -> bool {
        self.verbose
    }

    pub fn shared_secret(&self) -> &String {
        &self.secret
    }

    pub fn allow_localhost(&self) -> bool {
        self.allow_localhost
    }

    pub fn force_target(&self) -> &String {
        &self.force_target
    }

    pub fn force_https(&self) -> bool {
        self.force_https
    }

    pub fn api_addr(&self) -> &String {
        &self.api_addr
    }

    pub fn api_port(&self) -> usize {
        self.api_port
    }

    pub fn is_paging_enabled(&self) -> bool {
        self.enable_paging
    }

    pub fn enable_auth(&self) -> bool {
        self.enable_auth
    }

    pub fn is_scope_enabled(&self) -> bool {
        !self.scope.is_empty()
    }

    pub fn scope(&self) -> &str {
        &self.scope
    }

    #[cfg(test)]
    pub fn set_shared_secret(&mut self, s: impl ToString) {
        self.secret = s.to_string();
    }

    #[cfg(test)]
    pub fn set_directory(&mut self, s: impl ToString) {
        self.directory = s.to_string();
    }
}
