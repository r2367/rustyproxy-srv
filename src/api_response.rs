const CRLF: &[u8; 2] = b"\r\n";

#[derive(Debug, Default, Eq, PartialEq)]
pub struct ApiResponse {
    version: HttpVersion,
    status: ApiResponseStatus,
    headers: ApiResponseHeaders,
    body: ApiResponseBody,
}

#[derive(Debug, Default, Eq, PartialEq)]
pub struct ApiResponseBody {
    content_type: Option<ApiResponseHeader>,
    content: Option<String>,
}

#[derive(Debug, Default, Eq, PartialEq)]
pub struct ApiResponseHeaders {
    headers: Vec<ApiResponseHeader>,
}

#[derive(Debug, Eq, PartialEq)]
pub struct ApiResponseHeader {
    key: String,
    value: String,
}

#[derive(Debug, Eq, PartialEq)]
pub enum ApiResponseStatus {
    Ok,
    NotFound,
    Forbidden,
    MethodNotAllowed,
    InternalServerError,
    BadRequest,
}

#[derive(Debug, Eq, PartialEq)]
pub enum HttpVersion {
    Http11,
}

impl ApiResponseStatus {
    fn as_bytes(&self) -> Vec<u8> {
        match self {
            ApiResponseStatus::Ok => b"200 OK".to_vec(),
            ApiResponseStatus::NotFound => b"404 NOT FOUND".to_vec(),
            ApiResponseStatus::Forbidden => b"403 FORBIDDEN".to_vec(),
            ApiResponseStatus::MethodNotAllowed => b"405 METHOD NOT ALLOWED".to_vec(),
            ApiResponseStatus::InternalServerError => b"500 INTERNAL SERVER ERROR".to_vec(),
            ApiResponseStatus::BadRequest => b"400 BAD REQUEST".to_vec(),
        }
    }

    fn from_usize(s: usize) -> Self {
        match s {
            405 => ApiResponseStatus::MethodNotAllowed,
            404 => ApiResponseStatus::NotFound,
            403 => ApiResponseStatus::Forbidden,
            200 => ApiResponseStatus::Ok,
            500 => ApiResponseStatus::InternalServerError,
            400 => ApiResponseStatus::BadRequest,
            _ => ApiResponseStatus::InternalServerError,
        }
    }

    pub fn as_str(&self) -> &'static str {
        match self {
            ApiResponseStatus::Ok => "200 OK",
            ApiResponseStatus::NotFound => "404 NOT FOUND",
            ApiResponseStatus::Forbidden => "403 FORBIDDEN",
            ApiResponseStatus::MethodNotAllowed => "405 METHOD NOT ALLOWED",
            ApiResponseStatus::InternalServerError => "500 INTERNAL SERVER ERROR",
            ApiResponseStatus::BadRequest => "400 BAD REQUEST",
        }
    }
}

impl Default for ApiResponseStatus {
    fn default() -> Self {
        Self::Ok
    }
}

impl Default for HttpVersion {
    fn default() -> Self {
        Self::Http11
    }
}

impl HttpVersion {
    fn as_bytes(&self) -> Vec<u8> {
        match self {
            Self::Http11 => b"HTTP/1.1 ".to_vec(),
        }
    }
}

impl ApiResponseBody {
    fn as_bytes(&self) -> Vec<u8> {
        let mut out = vec![];

        if let Some(content) = &self.content {
            out.extend_from_slice(content.as_bytes());
        }

        out
    }
}

impl ApiResponseHeaders {
    fn as_bytes(&self) -> Vec<u8> {
        let mut out = vec![];
        for h in &self.headers {
            out.extend_from_slice(format!("{}: {}", h.key, h.value).as_bytes());
            out.extend_from_slice(CRLF);
        }
        out
    }

    pub fn with_header(&mut self, key: String, value: String) {
        self.headers.push(ApiResponseHeader { key, value });
    }
}

impl ApiResponse {
    pub fn as_bytes(&self) -> Vec<u8> {
        let mut out = vec![];
        out.append(&mut self.version.as_bytes());
        out.append(&mut self.status.as_bytes());
        out.extend_from_slice(CRLF);
        out.append(&mut self.headers.as_bytes());
        if self.body.content.is_some() {
            let mut content = self.body.as_bytes();
            out.extend_from_slice(format!("Content-Length: {}", content.len()).as_bytes());
            out.extend_from_slice(CRLF);
            if let Some(ctype) = &self.body.content_type {
                out.extend_from_slice(format!("Content-Type: {}", ctype.value).as_bytes());
                out.extend_from_slice(CRLF);
            }
            out.extend_from_slice(CRLF);
            out.append(&mut content);
        }
        out
    }

    pub fn with_status(mut self, s: usize) -> Self {
        self.status = ApiResponseStatus::from_usize(s);
        self
    }

    pub fn with_body(mut self, b: impl ToString) -> Self {
        self.body.content = Some(b.to_string());
        self
    }

    pub fn with_content_type(mut self, ctype: impl ToString) -> Self {
        let header = ApiResponseHeader {
            key: "Content-Type".to_string(),
            value: ctype.to_string(),
        };
        self.body.content_type = Some(header);
        self
    }

    pub fn with_json(self, json: impl ToString) -> Self {
        self.with_content_type("application/json").with_body(json)
    }

    pub fn with_html(self, html: impl ToString) -> Self {
        self.with_content_type("text/html").with_body(html)
    }

    pub fn headers_mut(&mut self) -> &mut ApiResponseHeaders {
        &mut self.headers
    }

    pub fn status(&self) -> &ApiResponseStatus {
        &self.status
    }
}

#[cfg(test)]
mod tests {
    use crate::api_response::ApiResponse;
    #[test]
    fn not_found_404() {
        let resp = ApiResponse::default().with_status(404);
        assert_eq!(resp.as_bytes(), b"HTTP/1.1 404 NOT FOUND\r\n");
    }

    #[test]
    fn ok_200() {
        let resp = ApiResponse::default().with_status(200);
        assert_eq!(resp.as_bytes(), b"HTTP/1.1 200 OK\r\n");
    }

    #[test]
    fn isr_500() {
        let resp = ApiResponse::default().with_status(500);
        assert_eq!(resp.as_bytes(), b"HTTP/1.1 500 INTERNAL SERVER ERROR\r\n");
    }

    #[test]
    fn mna_405() {
        let resp = ApiResponse::default().with_status(405);
        assert_eq!(resp.as_bytes(), b"HTTP/1.1 405 METHOD NOT ALLOWED\r\n");
    }

    #[test]
    fn forbidden_403() {
        let resp = ApiResponse::default().with_status(403);
        assert_eq!(resp.as_bytes(), b"HTTP/1.1 403 FORBIDDEN\r\n");
    }

    #[test]
    fn br_400() {
        let resp = ApiResponse::default().with_status(400);
        assert_eq!(resp.as_bytes(), b"HTTP/1.1 400 BAD REQUEST\r\n");
    }

    #[test]
    fn json() {
        let resp = ApiResponse::default()
            .with_status(200)
            .with_json("{\"test\":true}");
        assert_eq!(resp.as_bytes(), b"HTTP/1.1 200 OK\r\nContent-Length: 13\r\nContent-Type: application/json\r\n\r\n{\"test\":true}");
    }
}
