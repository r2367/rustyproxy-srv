use log::debug;

use nomhttp::http_chunk::HttpChunk;
use nomhttp::http_header::HttpHeader;
use nomhttp::http_response::HttpResponse;
use nomhttp::http_response_status::HttpResponseStatus;
use nomhttp::http_transfer_encoding::HttpTransferEncoding;

use nom::bytes::streaming::{take, take_until};
use nom::combinator::map_res;
use nom::IResult;
use nom_bufreader::bufreader::BufReader;
use nom_bufreader::Parse;

use std::{
    error::Error,
    io::{Read, Write},
};

use crate::parsers::*;

type ParseResponseRetType = (HttpResponseStatus, Vec<HttpHeader>, Vec<u8>, Vec<u8>);

fn nom_parse_response(i: &[u8]) -> IResult<&[u8], ParseResponseRetType, ()> {
    debug!("Entering nom_parse_response: {:?}", i);
    let copy = i;
    let (i, status_line) = parse_status_line(i)?;
    debug!("Status line parsed");
    let (body_au8, headers) = parse_headers(i)?;
    debug!("Headers parsed");
    debug!("{:?}", headers);
    let body: Vec<u8> = body_au8.to_vec();

    let head_len = copy.len() - body_au8.len();
    let raw = copy[..head_len].to_vec();

    let r = (status_line, headers, body, raw);

    Ok((body_au8, r))
}

fn handle_response_head<T>(reader: &mut BufReader<T>) -> Option<ParseResponseRetType>
where
    T: Read + Write,
{
    debug!("Entering handle_response_head");
    let r = reader.parse(nom_parse_response).ok()?;
    Some(r)
}

pub fn handle_response<T, Y>(
    rstreamreader: &mut BufReader<T>,
    mut s: Y,
) -> Result<HttpResponse, Box<dyn Error>>
where
    T: Read + Write,
    Y: Read + Write,
{
    debug!("Entering handle_response");
    let resp = handle_response_head(rstreamreader);
    let resp = match resp {
        None => return Err("Could not parse response".into()),
        Some(x) => {
            debug!("Got response head");
            let (status, headers, _, head) = x;
            let debug_head = head.clone();
            if s.write_all(&head).is_err() {
                return Err("Error while writing response head".into());
            }
            debug!("Written response head back to client");
            let mut r = HttpResponse::new(status, headers, vec![], head);

            if let Some(clen) = r.content_length() {
                debug!("Response had content-length: {}", clen);
                let body = r.body_mut();
                let mut data = read_until_size(rstreamreader, (clen - body.len()) as u64)?;
                body.append(&mut data);
                debug!("Body before send: {:?}", &body);
                if let Err(e) = s.write_all(body) {
                    return Err(format!("Error while writing body to client: {}", e).into());
                }
            } else if r.transfer_encoding() == &HttpTransferEncoding::Chunked {
                debug!("Response is chunk encoded");
                let body = r.body_mut();
                loop {
                    /* get chunk */
                    let chunk =
                        read_chunk(rstreamreader).ok_or(format!("Error while reading chunk: {:?}", debug_head))?;
                    /* write chunk to response object */
                    body.extend_from_slice(&chunk.raw()[..]);
                    /* write chunk to client */
                    if let Err(e) = s.write_all(chunk.raw()) {
                        return Err(format!("Error while writing chunks: {}", e).into());
                    }
                    /* check chunk size to exit the loop */
                    if matches!(chunk.size(), 0) {
                        break;
                    }
                }

                if &body[body.len()-5..] == [0x30, 0x0d, 0x0a, 0x0d, 0x0a] {
                    debug!("COMPLETE CHUNKED");
                } else {
                    debug!("INCOMPLETE CHUNKED: {:?}", &body[body.len()-5..]);
                }

            }

            r
        }
    };

    Ok(resp)
}

fn parse_chunk_header(i: &[u8]) -> IResult<&[u8], usize, ()> {
    let (i, header_au8) = take_until("\r\n")(i)?;

    let (_, size_s) = map_res(take(header_au8.len()), std::str::from_utf8)(header_au8)?;

    let size = usize::from_str_radix(size_s, 16).or(Err(nom::Err::Failure(())))?;

    let (i, _) = take(2u8)(i)?;

    Ok((i, size))
}

fn take_chunk_body(i: &[u8], size: usize) -> IResult<&[u8], Vec<u8>, ()> {
    debug!("Entering take_chunk_body");
    let (i, chunkbody) = take(size)(i)?;
    debug!("Body size: {}, remainder size: {}", chunkbody.len(), i.len());
    let (i, _) = take(2u8)(i)?;
    let mut body = Vec::with_capacity(chunkbody.len());
    body.extend_from_slice(chunkbody);
    body.extend_from_slice(&[0x0d, 0x0a]);

    Ok((i, body))
}

fn nom_parse_chunk(i: &[u8]) -> IResult<&[u8], HttpChunk, ()> {
    debug!("Entering nom_parse_chunk");
    let (rest, chunksize) = parse_chunk_header(i)?;
    debug!("Chunk size: {} - Remainder size: {}", chunksize, rest.len());
    let (rest, chunkbody) = take_chunk_body(rest, chunksize)?;
    debug!("Chunk body: {:?}", chunkbody);

    let chunk_raw = &i[..i.len() - rest.len()];
    let chunk = HttpChunk::new(chunk_raw.to_vec(), chunkbody, chunksize);

    Ok((rest, chunk))
}

fn read_chunk<T>(r: &mut BufReader<T>) -> Option<HttpChunk>
where
    T: Read + Write,
{
    let chunk = r.parse(nom_parse_chunk).ok()?;

    Some(chunk)
}
