use core::pin::Pin;
use log::{debug, info};
use nom_bufreader::bufreader::BufReader;
use nomhttp::http_method::HttpMethod;
use nomhttp::http_request::HttpRequest;
use openssl::{
    pkey::{PKey, Private},
    ssl::{Ssl, SslAcceptor, SslMethod, SslStream, SslVerifyMode},
    x509::X509,
};
use rusqlite::Connection;
use std::error::Error;
use std::io::Write;
use std::net::{TcpListener, TcpStream};
use std::thread;
use std::sync::mpsc;

use crate::note::Note;
use crate::api_response::ApiResponse;
use crate::config::Config;
use crate::inspector::Inspector;
use crate::request_handler::handle_request;
use crate::{is_auth, wrap};
use crate::sqlite_manager::{Msg, DataType, DataResult, PushDataType};

fn _handle_api(
    cert: X509,
    pair: PKey<Private>,
    ca_cert: X509,
    config: Config,
    sender: mpsc::Sender<Msg>,
    sender_api: mpsc::Sender<Msg>
) -> Result<(), Box<dyn Error>> {
    let addr = format!("{}:{}", config.api_addr(), config.api_port());
    let listener = TcpListener::bind(&addr)?;
    info!("Api listening on: {}", addr);

    for stream in listener.incoming() {
        let s = stream?;
        let cert = cert.clone();
        let pair = pair.clone();
        let ca_cert = ca_cert.clone();
        let cfg = config.clone();
        let tx = sender.clone();
        let tx_api = sender_api.clone();
        thread::spawn(move || handle_client(s, &cert, &pair, ca_cert, cfg, tx, tx_api));
    }

    Ok(())
}

pub fn handle_api(cert: X509, pair: PKey<Private>, ca_cert: X509, config: Config, sender: mpsc::Sender<Msg>, sender_api: mpsc::Sender<Msg>) -> bool {
    _handle_api(cert, pair, ca_cert, config, sender, sender_api).is_ok()
}

fn _handle_client(
    s: TcpStream,
    cert: &X509,
    pair: &PKey<Private>,
    ca_cert: X509,
    config: Config,
    sender: mpsc::Sender<Msg>,
    sender_api: mpsc::Sender<Msg>,
) -> Result<(), Box<dyn Error>> {
    let cacert = ca_cert.clone();
    let mut sslstream = wrap!(s, cert, pair, ca_cert);
    Pin::new(&mut sslstream).accept()?;
    let mut reader = BufReader::with_capacity(1024 * 1024, sslstream);
    let (method, path, version, headers, body, raw) =
        handle_request(&mut reader).ok_or("Error while parsing request")?;

    let req = HttpRequest::new(method, path, version, headers, body, raw, false);
    let resp = match req.path().chars().take(5).collect::<String>().as_str() {
        "/" => ApiResponse::default().with_html("<html><body>Hello world!</body></html>"),
        "/cert" | "/cacert" | "/cacert.der" | "/cacert.pem" | "/cert.der" | "/cert.pem" => {
            handle_cert_route(&cacert)?
        }
        "/api/" => handle_api_route(&req, config, sender, sender_api)?,
        _ => ApiResponse::default().with_status(404),
    };

    let mut stream = reader.into_inner();
    stream.write_all(&resp.as_bytes())?;

    stream.shutdown()?;
    debug!(
        "[API] {} {} [{}]",
        req.method(),
        req.path(),
        resp.status().as_str(),
    );
    Ok(())
}


fn handle_client(
    s: TcpStream,
    cert: &X509,
    pair: &PKey<Private>,
    ca_cert: X509,
    config: Config,
    sender: mpsc::Sender<Msg>,
    sender_api: mpsc::Sender<Msg>,
) -> bool {
    _handle_client(s, cert, pair, ca_cert, config, sender, sender_api).is_ok()
}

fn handle_cert_route(ca_cert: &X509) -> Result<ApiResponse, Box<dyn Error>> {
    let pem = ca_cert.to_pem()?;
    let mut resp = ApiResponse::default()
        .with_status(200)
        .with_body(String::from_utf8_lossy(&pem).to_string())
        .with_content_type(
            "application/x-x509-
    ca-cert",
        );
    let headers = resp.headers_mut();
    headers.with_header(
        "Content-Disposition".to_string(),
        "inline; filename=\"cacert.der\"".to_string(),
    );
    Ok(resp)
}

pub fn handle_api_route(r: &HttpRequest, config: Config, sender: mpsc::Sender<Msg>, sender_api: mpsc::Sender<Msg>) -> Result<ApiResponse, Box<dyn Error>> {
    let mut resp = if is_auth!(r, config.shared_secret()) {
        let parts = r
            .path()
            .split("/api/")
            .skip(1)
            .take(1)
            .map(|s| s.to_string())
            .collect::<String>()
            .split('/')
            .map(|s| s.to_string())
            .collect::<Vec<String>>();
        let resp = match parts[0].as_str() {
            "inspectors" => handle_inspector_api(r, parts, &config, sender, sender_api),
            "requests" => handle_requests_api(r, parts, &config, sender, sender_api),
            "websockets" => handle_websockets_api(r, parts, &config, sender, sender_api),
            "notes" => handle_notes_api(r, parts, &config, sender, sender_api),
            _ => ApiResponse::default().with_status(404),
        };
        resp
    } else {
        ApiResponse::default().with_status(403)
    };

    resp.headers_mut()
        .with_header("Access-Control-Allow-Origin".to_string(), "*".to_string());

    Ok(resp)
}

fn handle_notes_api(r: &HttpRequest, parts: Vec<String>, config: &Config, sender: mpsc::Sender<Msg>, sender_api: mpsc::Sender<Msg>) -> ApiResponse {
    match _handle_notes_api(r, parts, config, sender, sender_api) {
        Ok(r) => r,
        Err(e) => ApiResponse::default().with_status(500).with_json(format!(
            "{{\"error\": {}}}",
            json::stringify(format!("{:?}", e))
        )),
    }
}

fn _handle_notes_api(r: &HttpRequest, parts: Vec<String>, config: &Config, sender: mpsc::Sender<Msg>, sender_api: mpsc::Sender<Msg>) -> Result<ApiResponse, Box<dyn Error>> {
    match r.method() {
        HttpMethod::Get => list_or_get_notes(config, &parts, sender, sender_api),
        HttpMethod::Post => new_note(r, config, sender_api),
        HttpMethod::Patch => update_note(r, &parts, config, sender_api),
        _ => Ok(ApiResponse::default().with_status(405)),
    }
}

fn list_or_get_notes(_config: &Config, parts: &[String], _sender: mpsc::Sender<Msg>, sender_api: mpsc::Sender<Msg>) -> Result<ApiResponse, Box<dyn Error>> {
    let rid = parts
        .iter()
        .skip(1)
        .take(1)
        .next()
        .ok_or("Note ID seems to be missing from URL")?;

    let id = if !rid.is_empty() {
        rid.parse()?
    } else {
        0
    };
    let (tx, rx) = mpsc::channel();
    let msg = Msg::GiveMeData(DataType::Note(id), tx);
    sender_api.send(msg)?;
    if let DataResult::Note(result) = rx.recv()? {
        let out = serde_json::to_string(&result)?;
        Ok(ApiResponse::default().with_json(out))
    } else {
        Ok(ApiResponse::default()
            .with_status(500)
            .with_json("{\"Error\": \"Impossibru\"}"))
    }
}

fn new_note(r: &HttpRequest, _config: &Config, sender: mpsc::Sender<Msg>) -> Result<ApiResponse, Box<dyn Error>> {
    let note: Note =
        match serde_json::from_str(&r.body().iter().map(|&x| x as char).collect::<String>()) {
            Ok(i) => i,
            Err(_) => {
                return Ok(ApiResponse::default()
                    .with_status(400)
                    .with_json("{\"error\":\"Invalid or missing JSON\"}"))
            }
        };
    let msg = Msg::WriteNewData(PushDataType::Note(note));
    sender.send(msg)?;
    Ok(ApiResponse::default().with_status(200))
}

fn update_note(r: &HttpRequest, parts: &[String], _config: &Config, sender: mpsc::Sender<Msg>) -> Result<ApiResponse, Box<dyn Error>> {
    let rid = parts
        .iter()
        .skip(1)
        .take(1)
        .next()
        .ok_or("Note ID seems to be missing from URL")?;

    let id = if !rid.is_empty() {
        rid.parse()?
    } else {
        0
    };
    let mut note: Note =
        match serde_json::from_str(&r.body().iter().map(|&x| x as char).collect::<String>()) {
            Ok(i) => i,
            Err(_) => {
                return Ok(ApiResponse::default()
                    .with_status(400)
                    .with_json("{\"error\":\"Invalid or missing JSON\"}"))
            }
        };
    note.id = id;
    let msg = Msg::WriteNewData(PushDataType::Note(note));
    sender.send(msg)?;
    Ok(ApiResponse::default().with_status(200))
}


fn handle_websockets_api(r: &HttpRequest, parts: Vec<String>, config: &Config, sender: mpsc::Sender<Msg>, sender_api: mpsc::Sender<Msg>) -> ApiResponse {
    match _handle_websockets_api(r, parts, config, sender, sender_api) {
        Ok(r) => r,
        Err(e) => ApiResponse::default().with_status(500).with_json(format!(
            "{{\"error\": {}}}",
            json::stringify(format!("{:?}", e))
        )),
    }
}

fn _handle_websockets_api(r: &HttpRequest, parts: Vec<String>, config: &Config, sender: mpsc::Sender<Msg>, sender_api: mpsc::Sender<Msg>) -> Result<ApiResponse, Box<dyn Error>> {
    match r.method() {
        HttpMethod::Get => list_or_get_websockets(config, &parts, sender, sender_api),
        _ => Ok(ApiResponse::default().with_status(405)),
    }
}

fn list_or_get_websockets(_config: &Config, parts: &[String], _sender: mpsc::Sender<Msg>, sender_api: mpsc::Sender<Msg>) -> Result<ApiResponse, Box<dyn Error>> {
    let rid = parts
        .iter()
        .skip(1)
        .take(1)
        .next()
        .ok_or("WebSocket ID seems to be missing from URL")?;

    let id = if !rid.is_empty() {
        rid.parse()?
    } else {
        0
    };
    let (tx, rx) = mpsc::channel();
    let msg = Msg::GiveMeData(DataType::Websocket(id), tx);
    sender_api.send(msg)?;
    if let DataResult::Websocket(result) = rx.recv()? {
        let out = serde_json::to_string(&result)?;
        Ok(ApiResponse::default().with_json(out))
    } else {
        Ok(ApiResponse::default()
            .with_status(500)
            .with_json("{\"Error\": \"Impossibru\"}"))
    }
}

fn handle_inspector_api(r: &HttpRequest, parts: Vec<String>, config: &Config, sender: mpsc::Sender<Msg>, sender_api: mpsc::Sender<Msg>) -> ApiResponse {
    match _handle_inspector_api(r, parts, config, sender, sender_api) {
        Ok(r) => r,
        Err(e) => ApiResponse::default().with_status(500).with_json(format!(
            "{{\"error\": {}}}",
            json::stringify(format!("{:?}", e))
        )),
    }
}

fn _handle_inspector_api(
    r: &HttpRequest,
    parts: Vec<String>,
    config: &Config,
    sender: mpsc::Sender<Msg>,
    sender_api: mpsc::Sender<Msg>
) -> Result<ApiResponse, Box<dyn Error>> {
    match r.method() {
        HttpMethod::Get => list_or_get_inspectors(config, &parts, sender, sender_api),
        HttpMethod::Patch => update_inspector(r, config, &parts),
        HttpMethod::Post => new_inspector(r, config, sender),
        HttpMethod::Delete => del_inspector(config, &parts),
        _ => Ok(ApiResponse::default().with_status(405)),
    }
}

fn list_or_get_inspectors(
    _config: &Config,
    parts: &[String],
    _sender: mpsc::Sender<Msg>,
    sender_api: mpsc::Sender<Msg>
) -> Result<ApiResponse, Box<dyn Error>> {
    let rid = parts
        .iter()
        .skip(1)
        .take(1)
        .next()
        .ok_or("Inspector ID seems to be missing from URL")?;

    let id = if !rid.is_empty() {
        rid.parse()?
    } else {
        0
    };

    let (tx, rx) = mpsc::channel();
    let msg = Msg::GiveMeData(DataType::Inspector(id), tx);
    sender_api.send(msg)?;
    if let DataResult::Inspector(result) = rx.recv()? {
        let out = serde_json::to_string(&result)?;
        Ok(ApiResponse::default().with_json(out))
    } else {
        Ok(ApiResponse::default()
            .with_status(500)
            .with_json("{\"Error\": \"Impossibru\"}"))
    }
}

fn update_inspector(
    r: &HttpRequest,
    config: &Config,
    parts: &[String],
) -> Result<ApiResponse, Box<dyn Error>> {
    let rid = parts
        .iter()
        .skip(1)
        .take(1)
        .next()
        .ok_or("Inspector ID seems to be missing from URL")?;

    if !rid.is_empty() {
        let id = rid;
        let inspector: Inspector =
            serde_json::from_str(&r.body().iter().map(|&x| x as char).collect::<String>())?;
        let ssl = if inspector.ssl {
            "1".to_string()
        } else {
            "0".to_string()
        };
        let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
        let mut stmt = conn.prepare("UPDATE inspectors SET request=?, response=?, modified_request=?, new_response=?, ssl=?, target=?, bf_results=?, bf_request=? WHERE id=? RETURNING id")?;
        let ri = stmt.query_row(
            [
                inspector.request,
                inspector.response,
                inspector.modified_request,
                inspector.new_response,
                ssl,
                inspector.target,
                serde_json::to_string(&inspector.bf_results)?,
                inspector.bf_request,
                id.to_string(),
            ],
            |row| {
                let i: usize = row.get(0)?;
                Ok(i)
            },
        );
        match ri {
            Ok(i) => {
                Ok(ApiResponse::default().with_json(format!("{{\"id\": {}}}", json::stringify(i))))
            }
            Err(e) => match e {
                rusqlite::Error::QueryReturnedNoRows => Ok(ApiResponse::default()
                    .with_status(404)
                    .with_json("{\"error\": \"bad ID\"}")),
                _ => Err(e.into()),
            },
        }
    } else {
        Ok(ApiResponse::default()
            .with_status(404)
            .with_json("{\"error\": \"Missing ID\"}"))
    }
}

fn new_inspector(r: &HttpRequest, _config: &Config, sender: mpsc::Sender<Msg>) -> Result<ApiResponse, Box<dyn Error>> {
    let body = &r.body().iter().map(|&x| x as char).collect::<String>();
    debug!("Got request: {}", &body);
    let inspector: Inspector =
        match serde_json::from_str(&r.body().iter().map(|&x| x as char).collect::<String>()) {
            Ok(i) => i,
            Err(e) => {
                debug!("Got error: {}", e);
                return Ok(ApiResponse::default()
                    .with_status(400)
                    .with_json("{\"error\":\"Invalid or missing JSON\"}"))
            }
        };
    let msg = Msg::WriteNewData(PushDataType::Inspector(inspector));
    sender.send(msg)?;
    //Ok(ApiResponse::default().with_json(format!("{{\"id\": {}}}", json::stringify(i))))
    Ok(ApiResponse::default().with_status(200))
}

fn del_inspector(config: &Config, parts: &[String]) -> Result<ApiResponse, Box<dyn Error>> {
    let rid = parts
        .iter()
        .skip(1)
        .take(1)
        .next()
        .ok_or("Inspector ID seems to be missing from URL")?;

    if !rid.is_empty() {
        let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
        let mut stmt = conn.prepare("DELETE FROM inspectors WHERE id=? RETURNING id")?;
        let ri = stmt.query_row([rid], |row| {
            let i: usize = row.get(0)?;
            Ok(i)
        });
        match ri {
            Ok(i) => {
                Ok(ApiResponse::default().with_json(format!("{{\"id\": {}}}", json::stringify(i))))
            }
            Err(e) => match e {
                rusqlite::Error::QueryReturnedNoRows => Ok(ApiResponse::default()
                    .with_status(404)
                    .with_json("{\"error\": \"bad ID\"}")),
                _ => Err(e.into()),
            },
        }
    } else {
        Ok(ApiResponse::default()
            .with_status(404)
            .with_json("{\"error\": \"Missing ID\"}"))
    }
}

fn handle_requests_api(r: &HttpRequest, parts: Vec<String>, config: &Config, sender: mpsc::Sender<Msg>, sender_api: mpsc::Sender<Msg>) -> ApiResponse {
    match _handle_requests_api(r, parts, config, sender, sender_api) {
        Ok(r) => r,
        Err(e) => ApiResponse::default().with_status(500).with_json(format!(
            "{{\"error\": {}}}",
            json::stringify(format!("{:?}", e))
        )),
    }
}

fn _handle_requests_api(
    r: &HttpRequest,
    parts: Vec<String>,
    config: &Config,
    sender: mpsc::Sender<Msg>,
    sender_api: mpsc::Sender<Msg>
) -> Result<ApiResponse, Box<dyn Error>> {
    match r.method() {
        HttpMethod::Get => list_or_get_requests(config, &parts, sender, sender_api),
        HttpMethod::Delete => del_requests(config, &parts),
        _ => Ok(ApiResponse::default().with_status(405)),
    }
}

fn list_or_get_requests(_config: &Config, parts: &[String], _sender: mpsc::Sender<Msg>, sender_api: mpsc::Sender<Msg>) -> Result<ApiResponse, Box<dyn Error>> {
    let last_id = parts
        .iter()
        .skip(1)
        .take(1)
        .next()
        .ok_or("Request ID seems to be missing from URL")?;
    let id = if !last_id.is_empty() {
        last_id.parse()?
    } else {
        0
    };
    let (tx, rx) = mpsc::channel();
    let msg = Msg::GiveMeData(DataType::History(id), tx);
    sender_api.send(msg)?;
    if let DataResult::History(result) = rx.recv()? {
        let out = serde_json::to_string(&result)?;
        Ok(ApiResponse::default().with_json(out))
    } else {
        Ok(ApiResponse::default()
            .with_status(500)
            .with_json("{\"Error\": \"Impossibru\"}"))
    }
}

fn del_requests(config: &Config, parts: &[String]) -> Result<ApiResponse, Box<dyn Error>> {
    let rid = parts
        .iter()
        .skip(1)
        .take(1)
        .next()
        .ok_or("Request id seems to be missing from URL")?;

    if !rid.is_empty() {
        let id = rid;
        let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
        let mut stmt = conn.prepare("DELETE FROM history WHERE id=? RETURNING id")?;
        let ri = stmt.query_row([id], |row| {
            let i: usize = row.get(0)?;
            Ok(i)
        });
        match ri {
            Ok(i) => {
                Ok(ApiResponse::default().with_json(format!("{{\"id\": {}}}", json::stringify(i))))
            }
            Err(e) => match e {
                rusqlite::Error::QueryReturnedNoRows => Ok(ApiResponse::default()
                    .with_status(404)
                    .with_json("{\"error\": \"bad ID\"}")),
                _ => Err(e.into()),
            },
        }
    } else {
        Ok(ApiResponse::default()
            .with_status(404)
            .with_json("{\"error\": \"Missing ID\"}"))
    }
}
