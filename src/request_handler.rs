use crate::parsers::*;

use nomhttp::http_header::HttpHeader;
use nomhttp::http_method::HttpMethod;
use nomhttp::http_version::HttpVersion;

use nom::bytes::streaming::{take, take_until, take_while};
use nom::character::is_alphanumeric;
use nom::combinator::map_res;
use nom::IResult;
use nom_bufreader::bufreader::BufReader;
use nom_bufreader::Parse;

use log::debug;
use std::{
    io::{Read, Write},
    str::FromStr,
};

fn nom_parse_request(i: &[u8]) -> IResult<&[u8], ParseRequestRetType, ()> {
    debug!("Entering nom_parse_request: {:?}", i);
    let raw = i.to_vec();
    let (i, method) = parse_method(i)?;
    debug!("Parsed method: {}", method);
    let (i, path) = parse_path(i)?;
    debug!("Parsed path: {}", path);
    let (i, version) = parse_version(i)?;
    debug!("Parsed version: {}", version);
    let (body, headers) = parse_headers(i)?;
    debug!("Parsed headers: {:?}", headers);

    let head_len = raw.len() - body.len();

    let osize = headers.iter().find(|x| x.name().to_lowercase() == "content-length").map(|x| x.value().parse::<usize>().unwrap());

    if let Some(size) = osize {
        if body.len() != size {
            debug!("INCOMPLETE: body.len() is {}, wanted {}, remains: {}", body.len(), size, size - body.len());
            return Err(nom::Err::Incomplete(nom::Needed::Size(core::num::NonZeroUsize::new(size - body.len()).unwrap())));
        } else {
            debug!("COMPLETE");
        }
    } else if let Some(h) = headers.iter().find(|x| x.name().to_lowercase().contains("transfert-encoding")) {
        if h.value().to_lowercase().contains("chunked") {
            if &body[body.len()-5..] == [0x30, 0x0d, 0x0a, 0x0d, 0x0a] {
                debug!("COMPLETE CHUNKED");
            } else {
                debug!("INCOMPLETE CHUNKED: {:?}", &body[body.len()-5..]);
                return Err(nom::Err::Incomplete(nom::Needed::Unknown));
            }
        }
    }

    let r = (
        method,
        path,
        version,
        headers,
        body.to_vec(),
        raw[..head_len].to_vec(),
    );
    debug!("Request is: {:?}", r);
    Ok((body, r))

}

pub fn handle_request<T>(reader: &mut BufReader<T>) -> Option<ParseRequestRetType>
where
    T: Read + Write,
{
    let r = reader.parse(nom_parse_request).ok()?;
    Some(r)
}

fn parse_method(i: &[u8]) -> IResult<&[u8], HttpMethod, ()> {
    let (i, method_au8) = take_while(is_alphanumeric)(i)?;
    let (_, method_s) = map_res(take(method_au8.len()), std::str::from_utf8)(method_au8)?;
    let Ok(method) = HttpMethod::from_str(method_s) else {
        return Err(nom::Err::Failure(()));
    };

    let (i, _) = take(1u8)(i)?; // take space

    Ok((i, method))
}

fn parse_path(i: &[u8]) -> IResult<&[u8], String, ()> {
    let (i, path_au8) = take_until(" ")(i)?;

    let path = path_au8.iter().map(|&b| b as char).collect();

    let (i, _) = take(1u8)(i)?; // take space

    Ok((i, path))
}

fn parse_version(i: &[u8]) -> IResult<&[u8], HttpVersion, ()> {
    let (i, version_au8) = take_until("\r\n")(i)?;
    let (_, version_s) = map_res(take(version_au8.len()), std::str::from_utf8)(version_au8)?;
    let version = HttpVersion::from_str(version_s).or(Err(nom::Err::Failure(())))?;

    let (i, _) = take(2u8)(i)?; // take CRLF after Http version

    Ok((i, version))
}

type ParseRequestRetType = (
    HttpMethod,
    String,
    HttpVersion,
    Vec<HttpHeader>,
    Vec<u8>,
    Vec<u8>,
);
