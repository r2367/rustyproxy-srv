use chrono::Local;
use clap::Parser;
use env_logger::Builder;
use log::{error, info, LevelFilter};
use rusqlite::Connection;
use std::io::Write;
use std::net::TcpListener;
use std::thread;
use std::{error::Error, fs, path::Path};
use std::sync::{Arc, Mutex, mpsc::channel};

mod api_manager;
mod api_response;
mod client_manager;
mod config;
mod history;
mod inspector;
mod websockets;
mod parsers;
mod request_handler;
mod response_handler;
mod ssl;
mod ssl_manager;
mod macros;
mod sqlite_manager;
mod note;

#[cfg(test)]
mod tests;

use crate::api_manager::handle_api;
use crate::sqlite_manager::handle_sqlite;
use crate::client_manager::client_manager;
use crate::config::Config;
use crate::ssl_manager::{get_ca_pair, get_host_pair};

fn main() -> Result<(), Box<dyn Error>> {
    let config = Config::parse();

    let log_filter = match config.debug() {
        true => LevelFilter::Debug,
        false => LevelFilter::Info,
    };

    Builder::new()
        .format(|buf, record| {
            writeln!(
                buf,
                "{} [{}] - {}",
                Local::now().format("%Y-%m-%dT%H:%M:%S"),
                record.level(),
                record.args()
            )
        })
        .filter(None, log_filter)
        .init();

    match Path::new(config.directory()).exists() {
        true => {}
        false => {
            fs::create_dir(config.directory())?;
        }
    }

    let conn = Connection::open(format!("{}/hist.db", config.directory()))?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS history(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        remote_addr TEXT,
        uri TEXT,
        method TEXT,
        params INTEGER,
        status INTEGER,
        size INTEGER,
        timestamp TEXT,
        raw BLOB,
        ssl INTEGER,
        response BLOB,
        response_time TEXT)",
        [],
    )?;

    conn.execute(
        "CREATE TABLE IF NOT EXISTS ca(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        hostname TEXT,
        cert TEXT,
        private TEXT)",
        [],
    )?;

    conn.execute(
        "CREATE TABLE IF NOT EXISTS inspectors(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        request BLOB,
        response BLOB,
        modified_request BLOB,
        new_response BLOB,
        ssl INTEGER,
        target TEXT,
        bf_results BLOB,
        bf_request BLOB)",
        [],
    )?;

    conn.execute(
        "CREATE TABLE IF NOT EXISTS websockets(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        stream_rep TEXT,
        speaker TEXT,
        ssl INTEGER,
        data BLOB)",
        [],
    )?;

    conn.execute(
        "CREATE TABLE IF NOT EXISTS notes(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT,
        body TEXT)",
        [],
    )?;

    let addr = config.full_addr();

    let listener = TcpListener::bind(&addr)?;
    info!("Socket bound on: {}", addr);

    let Ok((ca_cert, ca_key_pair)) = get_ca_pair(&conn) else {
        return Err("Somehow could get/create the CA Certs".into());
    };
    let apiaddr = format!("{}:{}", config.api_addr(), config.api_port());
    let Ok((cert, pair)) = get_host_pair(&conn, &apiaddr, &ca_cert, &ca_key_pair) else {
        return Err("Somehow could get/create cert for the API".into());
    };
    let ncacert = ca_cert.clone();
    let ncfg = config.clone();
    let counter = Arc::new(Mutex::new(0));
    let (sender, receiver) = channel();
    let (sender_api, receiver_api) = channel();
    let tx = sender.clone();
    thread::spawn(move || handle_api(cert, pair, ncacert, ncfg, tx, sender_api));
    let ncfg = config.clone();
    thread::spawn(move || handle_sqlite(ncfg, receiver, receiver_api));
    loop {
        if *counter.lock().unwrap() < 50 {
            *counter.lock().unwrap() += 1;
            let cc = counter.clone();
            let (s, _) = listener.accept()?;
            let ca_cert = ca_cert.clone();
            let ca_key_pair = ca_key_pair.clone();
            let cfg = config.clone();
            let tx = sender.clone(); 
            thread::spawn(move || {
                if let Err(e) = client_manager(s, ca_cert, ca_key_pair, cfg, tx) {
                    error!("Error while handling client: {}", e);
                }
                *cc.lock().unwrap() -= 1;
            });
            thread::sleep(std::time::Duration::from_millis(10));
        }
    }
}
